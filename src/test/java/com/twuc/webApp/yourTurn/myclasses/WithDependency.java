package com.twuc.webApp.yourTurn.myclasses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithDependency {
    @Autowired
    private Dependent dependent;

    public Dependent getDependent() {
        return dependent;
    }
}
