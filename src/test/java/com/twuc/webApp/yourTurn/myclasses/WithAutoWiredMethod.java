package com.twuc.webApp.yourTurn.myclasses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class WithAutoWiredMethod {
    private Dependent dependent;
    private AnotherDependent anotherDependent;
    private List<String> logs = new ArrayList<>();

    public WithAutoWiredMethod(Dependent dependent) {
        this.dependent = dependent;
        logs.add("dependent");
    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent) {
        this.anotherDependent = anotherDependent;
        logs.add("anotherDependent");
    }

    public Dependent getDependent() {
        return dependent;
    }

    public AnotherDependent getAnotherDependent() {
        return anotherDependent;
    }

    public List<String> getLogs() {
        return logs;
    }
}
