package com.twuc.webApp.yourTurn.myclasses;

public class SimpleDependent {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
