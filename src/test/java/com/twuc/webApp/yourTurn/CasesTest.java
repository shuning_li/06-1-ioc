package com.twuc.webApp.yourTurn;

import com.twuc.webApp.simplest.scanning.AnnotatedObjectWithoutDependency;
import com.twuc.webApp.yourTurn.myclasses.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class CasesTest {
    @Test
    void should_create_object_using_Annotate_application_context() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.yourTurn.myclasses"
        );

        WithoutDependency bean = context.getBean(WithoutDependency.class);

        assertNotNull(bean);
        assertSame(WithoutDependency.class, bean.getClass());
    }

    @Test
    void should_create_object_which_has_dependent_using_Annotate_application_context() throws NoSuchFieldException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.yourTurn.myclasses"
        );
        WithDependency bean = context.getBean(WithDependency.class);
        assertNotNull(bean.getDependent());
        assertNotNull(bean);
        assertSame(WithDependency.class, bean.getClass());
        assertSame(Dependent.class, bean.getDependent().getClass());
    }

    @Test
    void should_not_create_object_when_class_out_of_scanning_range() {
        Assertions.assertThrows(AssertionFailedError.class, () -> {
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                    "com.twuc.webApp.yourTurn.myclasses"
            );
            OutOfScanningScope bean = context.getBean(OutOfScanningScope.class);
            assertNotNull(bean);
            assertSame(WithDependency.class, bean.getClass());
        });
    }

    @Test
    void should_get_O_o() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.yourTurn.myclasses"
        );
        SimpleObject bean = context.getBean(SimpleObject.class);
        assertNotNull(bean);
        assertEquals("O_o", bean.getSimpleDependent().getName());
    }

    @Test
    void should_use_first_construct_to_express_relationship() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.yourTurn.myclasses"
        );
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);
        assertNotNull(bean);

        assertEquals(Dependent.class, bean.getDependent().getClass());
    }

    @Test
    void should_call_construct_and_initialize() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.yourTurn.myclasses"
        );
        WithAutoWiredMethod bean = context.getBean(WithAutoWiredMethod.class);

        assertEquals(Dependent.class, bean.getDependent().getClass());
        assertEquals(AnotherDependent.class, bean.getAnotherDependent().getClass());

        assertEquals("dependent", bean.getLogs().get(0));
        assertEquals("anotherDependent", bean.getLogs().get(1));

    }

    @Test
    void should_create_multiple_implementations() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.yourTurn.myclasses"
        );
        Map<String, InterfaceWithMultipleImpls> beansOfType = context.getBeansOfType(InterfaceWithMultipleImpls.class);
        assertSame(ImplementationA.class, beansOfType.get("implementationA").getClass());
        assertSame(ImplementationB.class, beansOfType.get("implementationB").getClass());
        assertSame(ImplementationC.class, beansOfType.get("implementationC").getClass());
    }
}
